/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Produit;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import service.ProduitService;

/**
 *
 * @author Dave
 */
@Path("/produitService")
public class ProduitServiceRessource {
    ProduitService pdtServ = new ProduitService();
    
    @POST
    @Path("/produitService/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addProduit(@PathParam("e") Produit e) {
        
        pdtServ.ajouter(e);
    }
    
    @POST
    @Path("/produitService/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateProduit(@PathParam("e") Produit e) {
        
        pdtServ.modifier(e);
    }
    
    @GET
    @Path("/produitService/find")
    @Produces("MediaType.APPLICATION_JSON")
    public Produit addProduit(@PathParam("id") Integer id) {
        
        return pdtServ.trouver(id);
    }
    
    @DELETE
    @Path("/produitService/delete")
    public void deleteProduit(@PathParam("id") Integer id) {
        
        pdtServ.supprimer(id);
    }
    
    @DELETE
    @Path("/produitService/delete")
    public void deleteProduit(@PathParam("e") Produit e) {
        
        pdtServ.supprimer(e);
    }
    
    @GET
    @Path("/produitService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Produit> getListe() {
        
        return pdtServ.lister();
    }
    
    @GET
    @Path("/produitService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Produit> getListe(@PathParam("dbt") int debut, @PathParam("nbre") int nombre) {
        
        return pdtServ.lister(debut, nombre);
    }
    
}
