/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Achat;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import service.AchatService;

/**
 *
 * @author ACER
 */
@Path("/achatService")
public class AchatServiceRessource {
    AchatService actServ = new AchatService();
    
    @POST
    @Path("/achatService/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addAchat(@PathParam("e") Achat e) {
        
        actServ.ajouter(e);
    }
    
    @POST
    @Path("/achatService/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateAchat(@PathParam("e") Achat e) {
        
        actServ.modifier(e);
    }
    
    @GET
    @Path("/achatService/find")
    @Produces("MediaType.APPLICATION_JSON")
    public Achat addAchat(@PathParam("id") Integer id) {
        
        return actServ.trouver(id);
    }
    
    @DELETE
    @Path("/achatService/delete")
    public void deleteAchat(@PathParam("id") Integer id) {
        
        actServ.supprimer(id);
    }
    
    @DELETE
    @Path("/achatService/delete")
    public void deleteAchat(@PathParam("e") Achat e) {
        
        actServ.supprimer(e);
    }
    
    @GET
    @Path("/achatService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Achat> getListe() {
        
        return actServ.lister();
    }
    
    @GET
    @Path("/achatService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Achat> getListe(@PathParam("dbt") int debut, @PathParam("nbre") int nombre) {
        
        return actServ.lister(debut, nombre);
    }
    
}

