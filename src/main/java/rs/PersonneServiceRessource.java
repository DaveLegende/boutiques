/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Personne;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import service.PersonneService;

/**
 *
 * @author Dave
 */
@Path("/personneService")
public class PersonneServiceRessource {
    PersonneService empServ = new PersonneService();
    
    @POST
    @Path("/personneService/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addPersonne(@PathParam("e") Personne e) {
        
        empServ.ajouter(e);
    }
    
    @POST
    @Path("/personneService/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updatePersonne(@PathParam("e") Personne e) {
        
        empServ.modifier(e);
    }
    
    @GET
    @Path("/personneService/find")
    @Produces("MediaType.APPLICATION_JSON")
    public Personne addPersonne(@PathParam("id") Integer id) {
        
        return empServ.trouver(id);
    }
    
    @DELETE
    @Path("/personneService/delete")
    public void deletePersonne(@PathParam("id") Integer id) {
        
        empServ.supprimer(id);
    }
    
    @DELETE
    @Path("/personneService/delete")
    public void deletePersonne(@PathParam("e") Personne e) {
        
        empServ.supprimer(e);
    }
    
    @GET
    @Path("/personneService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Personne> getListe() {
        
        return empServ.lister();
    }
    
    @GET
    @Path("/personneService/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Personne> getListe(@PathParam("dbt") int debut, @PathParam("nbre") int nombre) {
        
        return empServ.lister(debut, nombre);
    }
    
}


