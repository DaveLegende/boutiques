/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

/**
 *
 * @author Dave
 */
public class ProduitAchete {
    private int quantite ;
    private double remise;
    public Produit produit;

    ProduitAchete(Produit fruit1, int i, double d) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ProduitAchete() {
        this.quantite = 1;
        this.remise = 0.0;
    }
    
    
    public double getRemise() {
        return this.remise;
    }
    
    
    public ProduitAchete(int quantite, double remise, Produit produit) {
        this.quantite = quantite;
        this.remise = remise;
        this.produit = produit;
    }
    
    public double getPrixTotal() {
        return quantite * produit.getPrixUnitaire() * (10 - remise);
    }

    public long getId() {
        return this.produit.getId();
    }
}
