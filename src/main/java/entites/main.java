/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author ACER
 */
public class main {
    public static void main(String[] args) {
        Categorie alimentation = new Categorie(145, "Fruits", "FruitComplet");
        Categorie saine = new Categorie(146, "Legumes", "LegumesComplet");
        //Two dates
        LocalDate date1 = LocalDate.of(2021, 5, 8);
        LocalDate date2 = LocalDate.of(2021, 6, 10);
        //Four produit
        Produit fruit1 = new Produit(147, "fruit1", 14.6, date1, alimentation);
        Produit fruit2 = new Produit(148, "fruit2", 18.9, date2, alimentation);
        Produit legume1 = new Produit(151, "legume1", 7.6, date1, alimentation);
        Produit legume2 = new Produit(152, "legume2", 9.8, date2, alimentation);
        //Four Produit achete
        ProduitAchete fruit01 = new ProduitAchete(fruit1, 4, 0.5);
        ProduitAchete fruit02 = new ProduitAchete(fruit2, 2, 0.1);
        ProduitAchete legume01 = new ProduitAchete(legume1, 3, 0.8);
        ProduitAchete legume02 = new ProduitAchete(legume2, 5, 0.25);
        
        //List of produit
        ArrayList<ProduitAchete> array1 = new ArrayList<ProduitAchete>();
        array1.add(fruit01);
        array1.add(fruit02);
        array1.add(legume01);
        array1.add(legume02);
        
        //Achat
        Achat achat1 = new Achat(526, 0.60, date1, array1);
        //Result
        System.out.println(achat1.toString());
        System.out.println(achat1.getPrixTotal()); 
    }
}
