/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author ACER
 */
public class Employe extends Personne{
    private String cnss;
    List<Achat> employeAchats;
    LocalDate dateEmbauche;
    
    //Constructors
    public Employe(long id, String nom, String prenoms, LocalDate dateNaissance, String cnss) {
        super(id, nom, prenoms, dateNaissance);
        this.cnss = cnss;   
    }

    /**
     *
     */
    public Employe() {
        super();
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.cnss);
        hash = 19 * hash + Objects.hashCode(this.dateEmbauche);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employe employe = (Employe) obj;
        return Objects.equals(this.cnss, employe.cnss);
    }

    @Override
    public String toString() {
        return "Employe avec son cnss" + cnss + "est embauché le " + dateEmbauche ;
    }

}
