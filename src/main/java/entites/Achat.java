/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 *
 * @author ACER
 */
public class Achat {
    private long id;
    private LocalDateTime dateAchat;
    private double remise;
    List<Achat> achatsClient;
    List<ProduitAchete> achatProduit;

    Achat(int i, double d, LocalDate date1, ArrayList<ProduitAchete> array1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public void addAchat(ProduitAchete produitAchete) {
        achatProduit.add(produitAchete);
    }
    
    public List<ProduitAchete> getAchatProduit() {
        return this.achatProduit;
    }
    //Constructors
    public Achat() {
        this.remise = 0.0;
    }
    Achat(long id, LocalDateTime dateAchat, double remise) {
        this.id = id;
        this.dateAchat = dateAchat;
        this.remise = remise;
    }
    
    //Getters
    public double getRemisetotale() {
        return remise;
    }
    public double getPrixTotal() {
        double price = 0;
        for(ProduitAchete i : achatProduit){
            price += i.getPrixTotal();
        }
        return price * (1-remise);
    }

    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        hash = 23 * hash + Objects.hashCode(this.dateAchat);
        hash = 23 * hash + Objects.hashCode(this.achatProduit);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat achat = (Achat) obj;
        if (this.id != achat.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Achat " + id + "avec remise de " + remise + "paye le " + dateAchat + "; voici la liste de achats: " + achatProduit;
    }

    public int getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
