/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author ACER
 */
public class Produit {
    private long id;
    private String libelle;
    private double prixUnitaire;
    private LocalDate datePeremption;
    Categorie categorieProduit;
    List<ProduitAchete> produitAchete;

    public Produit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }
    
    public double getPrixUnitaire() {
        return this.prixUnitaire;
    }
    
    
    Produit(long id, String libelle, double prixUnitaire, LocalDate datePeremption, Categorie categorieProduit) {
        this.id = id;
        this.libelle = libelle;
        this.prixUnitaire = prixUnitaire;
        this.datePeremption = datePeremption;
        this.categorieProduit = categorieProduit;
    }
    
    public boolean estPerime() {
        Date date = new Date();
        if(date.getYear() <= datePeremption.getYear()) {
            return false;
        }
        else {
            return true;
        }
    }
    
    public boolean estPerime(LocalDate dateReference) {
        if(dateReference.getYear() <= datePeremption.getYear()) {
            System.out.println("Produit non périmé");
            return false;
        }
        else {
            System.out.println("Produit périmé");
            return true;
        }
        
    }
    
    @Override
    public String toString() {
        return "Produit " + id + "sous libelle : " + libelle + "de categorie " + categorieProduit + "vaut " + prixUnitaire + "l'un et sera perimé: " + datePeremption;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produit produit = (Produit) obj;
        if (!Objects.equals(this.id, produit.libelle)) {
            return false;
        }
        if (!Objects.equals(this.prixUnitaire, produit.datePeremption)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int hash = 4;
        hash = 43 * hash + Objects.hashCode(this.id);
        hash = 43 * hash + Objects.hashCode(this.libelle);
        hash = 43 * hash + Objects.hashCode(this.datePeremption);
        hash = 43 * hash + Objects.hashCode(this.prixUnitaire);
        return hash;
    }

    public long getId() {
        return this.id;
    }
}
