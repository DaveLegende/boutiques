/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Employe;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 * @author ACER
 */
@Path("/boutique")
public class EmployeService {
    private List<Employe> liste;
    
    public void ajouter(Employe p) {
        if(liste.contains(p)) {
            System.out.println("Le employe existe déja dans la liste");
        }
        else {
            liste.add(p);
        }
    }
    
    public void modifier(Employe p) {
        if(liste.contains(p)) {
            for(Employe employe : liste) {
                if(employe.equals(p)) {
                    employe = p;
                }
            }
        }
        else {
            System.out.println("Le Employe est introuvable");
        }
    }
    
    public Employe trouver(int id) {
        Employe employe = new Employe();
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Employe p : liste) {
                if(p.getId() == id) {
                    System.out.println("Employe trouvé");
                    employe = p;
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
        }
        return employe;
    }
    
    public void supprimer(Integer id) {
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Employe c : liste) {
                if(c.getId() == id) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public void supprimer(Employe e) {
        if(liste == null) {
            System.out.print("La Liste est vide");
        }
        else {
            for(Employe c : liste) {
                if(liste.contains(c)) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Employe Introuvable dans la liste");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public List<Employe> lister() {
        if(liste == null) {
            System.out.print("La Liste est vide");
            return null;
        }
        else {
            for(Employe c : liste) {
                System.out.println(""+c);
                
            }
        } 
        return liste;
    }
    
    public List<Employe> lister(int debut, int nombre) {
        
        int index = debut;
        List<Employe> maListe = null;
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            if(debut > liste.size() || nombre > liste.size()) {
                System.out.println("Taille de la liste est débordée");
            }
            else {
                if(index == debut) {
                    do {
                        maListe.add(liste.get(index));
                        index++;
                        nombre--;
                    }while(nombre == 0);
                }
            }
        }
        return new ArrayList<>((Collection<? extends Employe>) maListe.iterator());
    }
}

