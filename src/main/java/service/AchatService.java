/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Achat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 * @author Dave
 */
@Path("/boutique")
public class AchatService {
    private List<Achat> liste;
    
    public void ajouter(Achat p) {
        if(liste.contains(p)) {
            System.out.println("Le achat existe déja dans la liste");
        }
        else {
            liste.add(p);
        }
    }
    
    public void modifier(Achat p) {
        if(liste.contains(p)) {
            for(Achat achat : liste) {
                if(achat.equals(p)) {
                    achat = p;
                }
            }
        }
        else {
            System.out.println("Le Achat est introuvable");
        }
    }
    
    public Achat trouver(int id) {
        Achat achat = new Achat();
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Achat p : liste) {
                if(p.getId() == id) {
                    System.out.println("Achat trouvé");
                    achat = p;
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
        }
        return achat;
    }
    
    public void supprimer(Integer id) {
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            for(Achat c : liste) {
                if(c.getId() == id) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Id introuvable");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public void supprimer(Achat e) {
        if(liste == null) {
            System.out.print("La Liste est vide");
        }
        else {
            for(Achat c : liste) {
                if(liste.contains(c)) {
                    liste.remove(c);
                }
                else {
                    System.out.println("Achat Introuvable dans la liste");
                }
            }
            System.out.println(""+liste);
        }
    }
    
    public List<Achat> lister() {
        if(liste == null) {
            System.out.print("La Liste est vide");
            return null;
        }
        else {
            for(Achat c : liste) {
                System.out.println(""+c);
                
            }
        } 
        return liste;
    }
    
    public List<Achat> lister(int debut, int nombre) {
        
        int index = debut;
        List<Achat> maListe = null;
        
        if(liste == null) {
            System.out.println("La Liste est vide");
        }
        else {
            if(debut > liste.size() || nombre > liste.size()) {
                System.out.println("Taille de la liste est débordée");
            }
            else {
                if(index == debut) {
                    do {
                        maListe.add(liste.get(index));
                        index++;
                        nombre--;
                    }while(nombre == 0);
                }
            }
        }
        return new ArrayList<>((Collection<? extends Achat>) maListe.iterator());
    }
}
